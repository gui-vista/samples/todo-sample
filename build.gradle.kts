group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.5.30"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val guiVistaGuiVer = "0.5.0"
    val guiVistaGroupId = "io.gitlab.gui-vista"

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                cinterops.create("glib2")
                cinterops.create("gio2")
                cinterops.create("gtk3")
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaGuiVer")
            }
        }
        binaries {
            executable("todo") {
                entryPoint = "org.example.todo.main"
            }
        }
    }

    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                cinterops.create("glib2")
                cinterops.create("gio2")
                cinterops.create("gtk3")
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaGuiVer")
            }
        }
        binaries {
            executable("todo") {
                entryPoint = "org.example.todo.main"
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.5.30"
                val guiVistaCoreVer = "0.4.3"
                val guiVistaIoVer = "0.4.3"
                implementation(kotlin("stdlib", kotlinVer))
                implementation("$guiVistaGroupId:guivista-core:$guiVistaCoreVer")
                implementation("$guiVistaGroupId:guivista-io:$guiVistaIoVer")
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaGuiVer")
            }
        }
    }
}
