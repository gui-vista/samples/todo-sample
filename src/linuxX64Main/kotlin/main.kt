package org.example.todo

import gtk3.GtkWindowPosition
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.guiApplicationActivateHandler
import io.gitlab.guiVista.io.application.Application

fun main() {
    guiApplicationActivateHandler = ::activateApplication
    GuiApplication.create(id = "org.example.Todo").use {
        mainWin = MainWindow(this)
        assignDefaultActivateHandler()
        println("Application Status: ${run()}")
    }
}

@Suppress("UNUSED_PARAMETER")
private fun activateApplication(app: Application) {
    println("Starting Todo...")
    mainWin.createUi {
        title = "Todo"
        changePosition(GtkWindowPosition.GTK_WIN_POS_CENTER)
        visible = true
    }
}