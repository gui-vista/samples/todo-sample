package org.example.todo

import glib2.G_TYPE_BOOLEAN
import glib2.G_TYPE_STRING
import glib2.gpointer
import gtk3.GtkButton
import gtk3.GtkOrientation
import gtk3.GtkSelectionMode
import gtk3.GtkTreeSelection
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.ListStore
import io.gitlab.guiVista.gui.layout.ContainerBase
import io.gitlab.guiVista.gui.layout.boxLayout
import io.gitlab.guiVista.gui.tree.TreeModelIterator
import io.gitlab.guiVista.gui.widget.button.buttonWidget
import io.gitlab.guiVista.gui.widget.button.checkButtonWidget
import io.gitlab.guiVista.gui.widget.dataEntry.entryWidget
import io.gitlab.guiVista.gui.widget.display.labelWidget
import io.gitlab.guiVista.gui.widget.tree.*
import io.gitlab.guiVista.gui.window.AppWindow
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.staticCFunction

internal actual class MainWindow actual constructor(app: GuiApplication) : AppWindow(app) {
    private val completedCheckBtn by lazy { checkButtonWidget(label = "Completed") }
    private val msgLbl by lazy { labelWidget(text = "What needs to be done?") }
    private val listView by lazy { treeViewWidget(model = listStore) {
        selection.mode = GtkSelectionMode.GTK_SELECTION_SINGLE
        appendColumn(taskCol)
        appendColumn(completedCol)
        selection.connectChangedEvent(staticCFunction(::selectionChanged))
    } }
    private val listStore by lazy { ListStore.create(G_TYPE_STRING.toULong(), G_TYPE_BOOLEAN.toULong()) }
    private val taskCol by lazy { treeViewTextColumn(columnId = 0, enableSorting = true) {
        title = "Task"
        sortIndicator = true
    } }
    private val completedCol by lazy { treeViewToggleColumn(columnId = 1, enableSorting = true) {
        title = "Completed"
        sortIndicator = true
    } }
    private val addBtn by lazy { buttonWidget(label = "Add") {
        connectClickedEvent(staticCFunction(::addBtnClicked))
    } }
    private val removeBtn by lazy { buttonWidget(label = "Remove") {
        connectClickedEvent(staticCFunction(::removeBtnClicked))
    } }
    private val clearBtn by lazy { buttonWidget(label = "Clear") {
        connectClickedEvent(staticCFunction(::clearBtnClicked))
    } }
    private val saveBtn by lazy { buttonWidget(label = "Save") {
        connectClickedEvent(staticCFunction(::saveBtnClicked))
    } }
    private val taskTxt by lazy { entryWidget { placeholderText = "Enter task" } }

    private fun resetEntryFields() {
        taskTxt.text = ""
        completedCheckBtn.active = false
    }

    private fun addToListStore(todo: Todo) {
        val newRow = TreeModelIterator.create()
        listStore.append(newRow)
        listStore.changeStringValue(iter = newRow, column = 0, value = todo.task)
        listStore.changeIntValue(iter = newRow, column = 1, value = todo.completed.intValue)
        newRow.close()
    }

    private fun saveToListStore(row: TreeModelIterator, todo: Todo) = memScoped {
        listStore.changeStringValue(iter = row, column = 0, value = todo.task)
        listStore.changeIntValue(iter = row, column = 1, value = todo.completed.intValue)
    }

    actual fun addTodo() {
        if (taskTxt.text.trim().isNotEmpty()) {
            addToListStore(Todo(task = taskTxt.text, completed = completedCheckBtn.active))
            resetEntryFields()
            resetFocus()
        }
    }

    actual fun editTodo() {
        val (_, row, selectedNode) = listView.selection.fetchSelected()
        if (row != null && selectedNode) {
            saveToListStore(row, Todo(task = taskTxt.text, completed = completedCheckBtn.active))
        }
        row?.close()
        resetFocus()
    }

    actual fun removeAllTodos() {
        listStore.clear()
        resetEntryFields()
        resetFocus()
    }

    override fun createMainLayout(): ContainerBase = boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_VERTICAL) {
        spacing = 8
        marginTop = 5
        marginStart = 5
        marginEnd = 5
        appendChild(msgLbl)
        appendChild(listView)
        appendChild(boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL) {
            marginTop = 12
            spacing = 4
            appendChild(taskTxt)
            appendChild(completedCheckBtn)
        })
        appendChild(boxLayout(orientation = GtkOrientation.GTK_ORIENTATION_HORIZONTAL) {
            marginTop = 4
            marginBottom = 5
            spacing = 2
            appendChild(addBtn)
            appendChild(removeBtn)
            appendChild(saveBtn)
            appendChild(clearBtn)
        })
    }

    override fun resetFocus() {
        taskTxt.grabFocus()
    }

    actual fun removeTodo() {
        val (_, row, selectedNode) = listView.selection.fetchSelected()
        if (selectedNode && row != null) {
            listStore.remove(row)
            resetEntryFields()
            resetFocus()
        }
    }

    actual fun updateTaskTxt(newValue: String) {
        taskTxt.text = newValue
    }

    actual fun updateCompletedCheckBtn(newValue: Boolean) {
        completedCheckBtn.active = newValue
    }
}

@Suppress("UNUSED_PARAMETER")
private fun selectionChanged(selection: CPointer<GtkTreeSelection>, userData: gpointer) {
    val tmp = selection.toTreeSelection()
    val (model, iter, nodeSelected) = tmp.fetchSelected()
    if (model != null && iter != null && nodeSelected) {
        mainWin.updateTaskTxt(model.fetchValue(iter = iter, col = 0, valueType = G_TYPE_STRING.toULong()).fetchString())
        mainWin.updateCompletedCheckBtn(model.fetchValue(iter = iter, col = 1, valueType = G_TYPE_BOOLEAN.toULong())
            .fetchBoolean())
    }
}

@Suppress("UNUSED_PARAMETER")
private fun addBtnClicked(btn: CPointer<GtkButton>, userData: gpointer) {
    mainWin.addTodo()
}

@Suppress("UNUSED_PARAMETER")
private fun clearBtnClicked(btn: CPointer<GtkButton>, userData: gpointer) {
    mainWin.removeAllTodos()
}

@Suppress("UNUSED_PARAMETER")
private fun removeBtnClicked(btn: CPointer<GtkButton>, userData: gpointer) {
    mainWin.removeTodo()
}

@Suppress("UNUSED_PARAMETER")
private fun saveBtnClicked(btn: CPointer<GtkButton>, userData: gpointer) {
    mainWin.editTodo()
}
