package org.example.todo

import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.window.AppWindow

internal expect class MainWindow(app: GuiApplication) : AppWindow {
    fun addTodo()

    fun editTodo()

    fun removeAllTodos()

    fun removeTodo()

    fun updateTaskTxt(newValue: String)

    fun updateCompletedCheckBtn(newValue: Boolean)
}
