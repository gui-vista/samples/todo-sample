package org.example.todo

internal data class Todo(val task: String, val completed: Boolean)

internal val Boolean.intValue: Int
    get() = if (this) 1 else 0
internal lateinit var mainWin: MainWindow
